package controller

import (
	"errors"
	"fmt"
	"os/exec"
	"runtime"
	"strings"
)

//ErrorGetDataFromWhois ...
var ErrorGetDataFromWhois = errors.New("Error getting data from whois")

// GetCountryAndNameIP Método encargado de retornan el pais nombre del dueño de un servidor
func GetCountryAndNameIP(ip string) (string, string, error) {
	country, err := getDataFromWhois(ip, "Country")
	if err != nil {
		return "", "", err
	}
	orgName, err := getDataFromWhois(ip, "OrgName")
	if err != nil {
		return "", "", err
	}
	fmt.Println(ip)
	return country, orgName, nil
}

func getDataFromWhois(ip string, nameData string) (string, error) {
	command := "whois " + ip + " | grep -i " + nameData
	// command := strings.Replace("whois 204.79.197.212 | grep -i %nameData%;", "%nameData%", nameData, -1)
	cmd := exec.Command("sh", "-c", command)

	if runtime.GOOS == "windows" {
		cmd = exec.Command("tasklist")
	}
	out, err := cmd.CombinedOutput()

	if err != nil {
		return "", err
	}
	nameData = nameData + ":"
	data := strings.Replace(string(out), nameData, " ", -1) // Se elimina la palabra clave con la que se busca en el string retornado por whois
	data = strings.TrimSpace(data)                          //Se eliminan espacios en blanco
	return data, nil
}
