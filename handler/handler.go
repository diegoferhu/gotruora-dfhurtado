package handler

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/diegoferhu/gotruora/controller"
	"github.com/go-chi/chi"
)

//AdminRouter ...
func AdminRouter() chi.Router {
	r := chi.NewRouter()
	r.Route("/domains", func(r chi.Router) {
		r.Get("/{domain}", requestDomain)
		r.Get("/history", requestDomainsHistory)
	})

	err := http.ListenAndServe(":3000", r)
	if err != nil {
		fmt.Println("ListenAndServe:", err)
	}
	return r
}

func requestDomain(w http.ResponseWriter, r *http.Request) {
	domain := chi.URLParam(r, "domain")
	hostData, err := controller.GetDomainData(domain)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	hostJSON, err := json.Marshal(hostData)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	enableCors(&w)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(hostJSON)
}

func requestDomainsHistory(w http.ResponseWriter, r *http.Request) {
	history, err := controller.GetHistory()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	hostJSON, err := json.Marshal(history)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	enableCors(&w)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(hostJSON)
}

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}
