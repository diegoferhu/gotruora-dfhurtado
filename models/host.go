package models

// HostFromAPI Estructura de respuesta de la api
type HostFromAPI struct {
	Host            string `json:"host"`
	Port            int    `json:"port"`
	Protocol        string `json:"protocol"`
	Ispublic        bool   `json:"ispublic"`
	Status          string `json:"status"`
	StartTime       int64  `json:"startTime"`
	TestTime        int64  `json:"testTime"`
	EngineVersion   string `json:"engineVersion"`
	CriteriaVersion string `json:"criteriaVersion"`
	Endpoints       []Endpoint
}

// Endpoint Estructura de respuesta de la api
type Endpoint struct {
	IPAddress         string `json:"ipAddress"`
	ServerName        string `json:"serverName"`
	StatusMessage     string `json:"statusMessage"`
	Grade             string `json:"grade"`
	GradeTrustIgnored string `json:"gradeTrustIgnored"`
	HasWarnings       bool   `json:"hasWarnings"`
	IsExceptional     bool   `json:"isExceptional"`
	Progress          int    `json:"progress"`
	Duration          int    `json:"duration"`
	Delegation        int    `json:"delegation"`
	Country           string `json:"country"`
	Owner             string `json:"owner"`
}

// HostData Estructura de respuesta de la api
type HostData struct {
	ID               int    `json:"id"`
	Name             string `json:"name"`
	ServerChanged    bool   `json:"server_changed"`
	SslGrade         string `json:"ssl_grade"`
	PreviousSslGrade string `json:"previous_ssl_grade"`
	Logo             string `json:"logo"`
	Title            string `json:"title"`
	IsDown           bool   `json:"is_down"`
	TimeLastQuery    string `json:"time_last_query"`
	Servers          []Server
}

// Server Estructura de respuesta de la api
type Server struct {
	Addres   string `json:"ipAddress"`
	SslGrade string `json:"grade"`
	Country  string `json:"country"`
	Owner    string `json:"owner"`
}

// History Estructura de respuesta de la api para el historial
type History struct {
	Name     string `json:"name"`
	HostData HostData
}
