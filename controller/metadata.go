package controller

import (
	"errors"
	"fmt"
	"net/http"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

//ErrorGetDataFromMetadata ...
var ErrorGetDataFromMetadata = errors.New("Error getting metadata from webPage status different of 200")

// GetDataFromMetadata Método encargado de obtener la url del icono y el title del dominio buscado.
func GetDataFromMetadata(domain string) (string, string, error) {

	var (
		tagsIcons []string
		url       string
		urlIcon   string
		title     string
	)

	url = "http://" + domain
	res, err := http.Get(url) //Petición

	if err != nil {
		return "", "", err
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		fmt.Println(res.StatusCode)
		return "", "", ErrorGetDataFromMetadata
	}

	doc, err := goquery.NewDocumentFromReader(res.Body) // Se carga el html
	if err != nil {
		return "", "", err
	}

	//Busqueda de la url del icono
	doc.Find("link").Each(func(i int, s *goquery.Selection) {
		if name, _ := s.Attr("rel"); strings.Contains(name, "icon") {
			href, _ := s.Attr("href")
			if !(strings.Contains(href, "https://") || strings.Contains(href, "http://")) {
				href = url + "/" + href
			}
			tagsIcons = append(tagsIcons, href)
		}
	})
	if len(tagsIcons) > 0 {
		urlIcon = (tagsIcons[0])
	} else {
		urlIcon = ""
	}

	//Busqueda del título
	title = doc.Find("head").Find("title").Text()

	return urlIcon, title, nil
}
