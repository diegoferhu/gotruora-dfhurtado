package db

import (
	"database/sql"
	"errors"
	"log"

	"github.com/diegoferhu/gotruora/models"
	_ "github.com/lib/pq"
)

const dbDataConnection = "postgresql://adminTruora@localhost:26257/truora?sslmode=disable"

var (
	db *sql.DB
	id = 0
	//ErrorConnectionToDB ...
	ErrorConnectionToDB = errors.New("Error connecting to the database")
)

// InitDB Inicializacioón de la BD
func InitDB() {
	var err error
	dbDataConnection := dbDataConnection
	db, err := sql.Open("postgres", dbDataConnection)
	if err != nil {
		log.Fatal("error connecting to the database: ", err)
	}

	if _, err := db.Exec(
		"DROP TABLE IF EXISTS domains"); err != nil {
		log.Fatal(err)
	}
	if _, err := db.Exec(
		"DROP TABLE IF EXISTS servers"); err != nil {
		log.Fatal(err)
	}

	if _, err := db.Exec(
		"DROP TABLE IF EXISTS history"); err != nil {
		log.Fatal(err)
	}

	// Create the "domains" table.
	if _, err := db.Exec(
		// "CREATE TABLE IF NOT EXISTS domains (id INT PRIMARY KEY, name STRING, server_changed BOOLEAN, ssl_grade STRING, previous_ssl_grade STRING, logo STRING, title STRING, is_down BOOLEAN, time_last_time STRING)"); err != nil {
		"CREATE TABLE IF NOT EXISTS domains (id INT PRIMARY KEY DEFAULT unique_rowid(), name STRING, server_changed BOOLEAN, ssl_grade STRING, previous_ssl_grade STRING, logo STRING, title STRING, is_down BOOLEAN, time_last_time STRING)"); err != nil {
		log.Fatal(err)
	}

	// Create the "servers" table.
	if _, err := db.Exec(
		"CREATE TABLE IF NOT EXISTS servers (id INT PRIMARY KEY DEFAULT unique_rowid(), id_domain INT, addres STRING, ssl_grade STRING, country STRING, owner STRING)"); err != nil {
		log.Fatal(err)
	}

	// Create the "history" table.
	if _, err := db.Exec(
		"CREATE TABLE IF NOT EXISTS history (id INT PRIMARY KEY DEFAULT unique_rowid(), name STRING)"); err != nil {
		log.Fatal(err)
	}

	// // Insert two rows into the "domains" table.
	// if _, err := db.Exec(
	// 	"INSERT INTO domains (name, server_changed, ssl_grade, previous_ssl_grade, logo, title, is_down, time_last_time) VALUES ('truora.com', false, 'A', 'C', 'https://uploads-ssl.webflow.com/5b559a554de48fbcb01fd277/5b97f0ac932c3291fa40d053_icon32.png', 'Truora', false, '20190521011704')"); err != nil {
	// 	log.Fatal(err)
	// }

	// // Insert two rows into the "accounts" table.
	// if _, err := db.Exec(
	// 	"INSERT INTO servers (id_domain, addres, ssl_grade, country, owner) VALUES ( 1, '34.193.204.92', 'A', 'US', 'Amazon Technologies Inc.'), ( 1, '34.193.69.252', 'A', 'US', 'Amazon Technologies Inc.')"); err != nil {
	// 	log.Fatal(err)
	// }

	// // Insert two rows into the "history" table.
	// if _, err := db.Exec(
	// 	"INSERT INTO history (name) VALUES ('truora.com'), ('hotmail.com'), ('github.com')"); err != nil {
	// 	log.Fatal(err)
	// }

}

// GetHistory Método encargado de obtener el historial de dominios consultados ultimamente.
func GetHistory() ([]models.History, error) {
	dbDataConnection := dbDataConnection
	db, err := sql.Open("postgres", dbDataConnection)
	if err != nil {
		return nil, ErrorConnectionToDB
	}

	rows, err := db.Query("SELECT name FROM  history")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	histories := []models.History{}
	for rows.Next() {
		history := models.History{}
		if err := rows.Scan(&history.Name); err != nil {
			return nil, err
		}
		domain, err := GetDomain(history.Name)
		if err != nil {
			return nil, err
		}
		history.HostData = *domain
		histories = append(histories, history)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}

	// if len(histories) == 0 {
	// 	return histories, nil
	// }

	return histories, nil
}

//GetDomain Método encargado de retornar u dominio especifico por medio de su nombre, en la BD
func GetDomain(domain string) (*models.HostData, error) {
	dbDataConnection := dbDataConnection
	db, err := sql.Open("postgres", dbDataConnection)
	if err != nil {
		return nil, ErrorConnectionToDB
	}

	rows, err := db.Query("SELECT id, name, server_changed, ssl_grade, previous_ssl_grade, logo, title, is_down, time_last_time FROM domains WHERE name = $1", domain)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	hostdata := models.HostData{}
	for rows.Next() {
		if err := rows.Scan(&hostdata.ID, &hostdata.Name, &hostdata.ServerChanged, &hostdata.SslGrade, &hostdata.PreviousSslGrade, &hostdata.Logo, &hostdata.Title, &hostdata.IsDown, &hostdata.TimeLastQuery); err != nil {
			return nil, err
		}
	}
	servers, err := getAllServers(hostdata.ID)
	if err != nil {
		return nil, err
	}
	hostdata.Servers = servers

	return &hostdata, nil
}

// Método encargado de obtener todos los servidores de un dominio
func getAllServers(domain int) ([]models.Server, error) {
	dbDataConnection := dbDataConnection
	db, err := sql.Open("postgres", dbDataConnection)
	if err != nil {
		return nil, err
	}

	rows, err := db.Query("SELECT addres, ssl_grade, country, owner FROM servers WHERE id_domain = $1", domain)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	servers := []models.Server{}
	for rows.Next() {
		server := models.Server{}
		if err := rows.Scan(&server.Addres, &server.SslGrade, &server.Country, &server.Owner); err != nil {
			return nil, err
		}
		servers = append(servers, server)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}

	return servers, nil
}

// InsertDomain Método encargado de Insertar valores de un dominio en la BD
func InsertDomain(hostdata models.HostData) (int, error) {
	var id int
	dbDataConnection := dbDataConnection
	db, err := sql.Open("postgres", dbDataConnection)
	if err != nil {
		return 0, ErrorConnectionToDB
	}
	Name := hostdata.Name
	ServerChanged := hostdata.ServerChanged
	SslGrade := hostdata.SslGrade
	PreviousSslGrade := hostdata.PreviousSslGrade
	Logo := hostdata.Logo
	Title := hostdata.Title
	IsDown := hostdata.IsDown
	TimeLastQuery := hostdata.TimeLastQuery

	err = db.QueryRow("INSERT INTO domains (name, server_changed, ssl_grade, previous_ssl_grade, logo, title, is_down, time_last_time) VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING id", Name, ServerChanged, SslGrade, PreviousSslGrade, Logo, Title, IsDown, TimeLastQuery).Scan(&id)
	if err != nil {
		return 0, err
	}

	err = insertServers(id, hostdata.Servers)
	if err != nil {
		return 0, err
	}

	return id, nil
}

// InsertToHistory Método encargado de Insertar valores de un dominio consultado al historial
func InsertToHistory(domain string) error {
	var id int
	dbDataConnection := dbDataConnection
	db, err := sql.Open("postgres", dbDataConnection)
	if err != nil {
		return ErrorConnectionToDB
	}

	err = db.QueryRow("INSERT INTO history (name) VALUES ($1) RETURNING id", domain).Scan(&id)
	if err != nil {
		return err
	}
	return nil
}

// insertServers Método encargado de insertar los valores de los servidores
func insertServers(idDomain int, servers []models.Server) error {
	for index := 0; index < len(servers); index++ {
		dbDataConnection := dbDataConnection
		db, err := sql.Open("postgres", dbDataConnection)
		if err != nil {
			return ErrorConnectionToDB
		}
		err = db.QueryRow("INSERT INTO servers (id_domain, addres, ssl_grade, country, owner) VALUES ($1, $2, $3, $4, $5) RETURNING id", idDomain, servers[index].Addres, servers[index].SslGrade, servers[index].Country, servers[index].Owner).Scan(&id)
		if err != nil {
			return err
		}
	}
	return nil
}
