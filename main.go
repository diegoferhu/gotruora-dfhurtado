package main

import (
	"fmt"

	"github.com/diegoferhu/gotruora/db"
	"github.com/diegoferhu/gotruora/handler"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"

	_ "github.com/lib/pq"
)

func main() {
	db.InitDB()

	fmt.Println("Starting server on port :3000")
	r := chi.NewRouter()
	r.Use(middleware.Logger)

	r.Mount("/domains", handler.AdminRouter())
}
