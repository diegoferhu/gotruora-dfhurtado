package controller

import (
	"errors"
	"fmt"
	"reflect"
	"time"

	"github.com/diegoferhu/gotruora/db"
	"github.com/diegoferhu/gotruora/models"
)

const formatDateTime = "20060102150405" //Constante para el manejo del formato de tiempo a guardar en BD
var (
	status string
	// hostDataFromAPI = models.HostFromAPI{}
)

// GetHistory Método encargado de obtener el historial de los dominios consultados.
func GetHistory() ([]models.History, error) {
	var historyToReturn = []models.History{}

	historyToReturn, err := db.GetHistory()
	if err != nil {
		return nil, err
	}
	return historyToReturn, nil
}

// GetDomainData Método encargado de obtener la información del host a retornar en en la petición GET de un dominio especifico.
func GetDomainData(domain string) (*models.HostData, error) {
	var isEnableToCompareServerData = false

	hostDataToReturn := models.HostData{}
	hostDataFromBd, err := db.GetDomain(domain)
	if err != nil {
		return nil, err
	}

	now := time.Now().UTC()

	// Si se retorna data de DB
	if hostDataFromBd.Title != "" {
		lastTimeSaved, _ := time.Parse(formatDateTime, hostDataFromBd.TimeLastQuery)
		timeDiff := now.Sub(lastTimeSaved).Hours() // Se calcula la difernecia de tiempo entre lo guardado en Bd y el tiempo actula de la nueva petición
		// Si se ha pasado mas de una hora se vuelve a intentar hacer una nueva consulta.
		if timeDiff > 1 {
			hostDataToReturn.TimeLastQuery = now.Format(formatDateTime) // Para cuando toque guardar en BD la fecha con cambio de hora
			isEnableToCompareServerData = true
		} else { //Si no ha pasado mas de una hora de la ultima consulta se devuelve lo mismo de la DB
			hostDataToReturn = *hostDataFromBd
			hostDataToReturn.TimeLastQuery = now.Format(formatDateTime) // Se actualiza la hora de la ultima consulta
			hostDataToReturn.ID = hostDataFromBd.ID
			return &hostDataToReturn, nil
		}
	}

	hostDataFromAPI, err := tryGetHostData(domain) // Se obtiene la informacioón de un host por medio de la API externa
	if err != nil {
		return nil, err
	}
	fmt.Println("controllerGetHostData")

	urlIcon, title, err := GetDataFromMetadata(domain) // Se obtine los datos de la url del icono y el titulo del dominio consultado por el usuario.
	if err != nil {
		return nil, err
	}

	servers := setServerData(hostDataFromAPI.Endpoints)

	hostDataToReturn.PreviousSslGrade = hostDataFromBd.SslGrade //hostDataFromBd.SslGrade Toca llenarlo apartir de lo obtenido de BD
	hostDataToReturn.Logo = urlIcon
	hostDataToReturn.Title = title
	hostDataToReturn.Name = domain
	hostDataToReturn.TimeLastQuery = now.Format(formatDateTime) // Para cuando toque guardar en BD la fecha con cambio de hora

	errorWhoIs := setCountryOrgNameIP(servers)
	if errorWhoIs != nil {
		return nil, errorWhoIs
	}

	if isEnableToCompareServerData {
		// Si los servidores de la DB son iguales a los servidores de la api se coloca en false serverchanged y se de deja el mismo sslGrade
		if reflect.DeepEqual(servers, hostDataFromBd.Servers) {
			hostDataToReturn.SslGrade = hostDataFromBd.SslGrade
			hostDataToReturn.ServerChanged = false
			hostDataToReturn.Servers = hostDataFromBd.Servers
		} else {
			// Si los servidores de la DB son difernete a los servidores de la api se coloca en true serverchanged y se se averigua el nuevo minimo del servidor
			hostDataToReturn.SslGrade = getMinSslGrade(hostDataFromAPI.Endpoints)
			fmt.Println(hostDataToReturn.SslGrade)
			hostDataToReturn.ServerChanged = true
			hostDataToReturn.Servers = servers
			id, err := db.InsertDomain(hostDataToReturn)
			if err != nil {
				return nil, err
			}
			err = db.InsertToHistory(hostDataToReturn.Name)
			if err != nil {
				return nil, err
			}
			hostDataToReturn.ID = id
		}
	} else {
		// Se debe averiguar el MinsslGrade ya que no existe un registro anterior con el cual comparar
		hostDataToReturn.SslGrade = getMinSslGrade(hostDataFromAPI.Endpoints)
		hostDataToReturn.Servers = servers
		id, err := db.InsertDomain(hostDataToReturn)
		if err != nil {
			return nil, err
		}
		err = db.InsertToHistory(hostDataToReturn.Name)
		if err != nil {
			return nil, err
		}
		hostDataToReturn.ID = id
	}
	return &hostDataToReturn, nil
}

//tryGetHostData Método recursivo en espera de la respuesta de la API externa de ssllabs
func tryGetHostData(domain string) (models.HostFromAPI, error) {
	hostDataFromAPI, err := QueryHostData(domain) // Se obtiene la informacioón de un host por medio de la API externa
	if err != nil {
		return hostDataFromAPI, err
	}
	status = hostDataFromAPI.Status
	if status != "READY" {
		if status == "ERROR" {
			return hostDataFromAPI, errors.New("Error status data from API ssllabs")
		}
		// time.Sleep(time.Second * 10) // Para evitar hacer demasiadas peticiones
		tryGetHostData(domain)
	}
	return hostDataFromAPI, nil
}

func setServerData(Endpoints []models.Endpoint) []models.Server {
	var nuEndpoints = len(Endpoints)
	var arrServerData []models.Server
	for index := 0; index < nuEndpoints; index++ {
		server := models.Server{Addres: Endpoints[index].IPAddress, SslGrade: Endpoints[index].Grade}
		arrServerData = append(arrServerData, server)
	}
	return arrServerData
}

// getMinSslGrade Método encargado de obtener el arreglo de string del grado de ssl de los servidores.
func getMinSslGrade(Endpoints []models.Endpoint) string {
	var (
		arrSslGrade []string
		sslGrade    string
	)
	if len(Endpoints) <= 1 {
		sslGrade = Endpoints[0].Grade
	} else {
		for index := 0; index < len(Endpoints); index++ {
			arrSslGrade = append(arrSslGrade, Endpoints[index].Grade)
		}
		sslGrade = getMinSslGradeFromArray(arrSslGrade)
	}
	return sslGrade
}

// getMinSslGradeFromArray Método encargado de obtener el grado de ssl mas bajo de los servidores de un dominio
func getMinSslGradeFromArray(arrSslGrade []string) string {
	var (
		arrMatchSslGrade []int
		sslGrade         string
	)

	// Se hace match de los valores alfanumericos de ssl a números.
	//dfhurtado revisar que hay servidores que retornan A+************************************************
	for index := 0; index < len(arrSslGrade); index++ {
		num := int([]rune(arrSslGrade[index])[0] - 65)
		arrMatchSslGrade = append(arrMatchSslGrade, num)
	}

	//Se hace la comparación y obtenien el ssl de menor grado
	sslGrade = arrSslGrade[0]
	min := arrMatchSslGrade[0]
	for index := 0; index < len(arrMatchSslGrade); index++ {
		if arrMatchSslGrade[index] < min {
			min = arrMatchSslGrade[index]
			sslGrade = arrSslGrade[index]
		}
	}
	return sslGrade
}

// setCountryOrgNameIP Método encargado de colocar los valores de pais y dueño de los endPoints de un dominio
func setCountryOrgNameIP(Servers []models.Server) error {
	for index := 0; index < len(Servers); index++ {
		// arrSslGrade = append(arrSslGrade, Servers[index].SslGrade)
		country, owner, err := GetCountryAndNameIP(Servers[index].Addres)
		if err != nil {
			return err
		}
		Servers[index].Country = country
		Servers[index].Owner = owner
	}
	return nil
}
