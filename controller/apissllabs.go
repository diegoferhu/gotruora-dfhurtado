package controller

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/diegoferhu/gotruora/models"
)

// QueryHostData método encargado de odtener la informacioón del host del dominio pasado
func QueryHostData(domain string) (models.HostFromAPI, error) {
	host := models.HostFromAPI{}

	url := "https://api.ssllabs.com/api/v3/analyze?host=" + domain
	spaceClient := http.Client{
		Timeout: time.Second * 20,
	}

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return host, err
	}

	res, getErr := spaceClient.Do(req)
	if getErr != nil {
		return host, getErr
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		return host, readErr
	}

	jsonErr := json.Unmarshal(body, &host)
	if jsonErr != nil {
		return host, jsonErr
	}

	return host, nil
}
